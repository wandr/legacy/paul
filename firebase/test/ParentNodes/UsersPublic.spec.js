/**
 * Created by Russell on 1/23/2016.
 */
/*jslint node: true */
"use strict";
var chai = require('chai'),
    expect = chai.expect,
    targaryen = require('targaryen');
chai.use(targaryen.chai);

// Auths.
var user1 = {uid: '-noVis01', provider: 'password'};
var user2 = {uid: '-noVis02', provider: 'password'};
var admin = {uid: 'admin', master: true, provider: 'custom'};

// Tests.
it('shouldn\'t let unauthenticated users read or write', function () {
    expect(targaryen.users.unauthenticated)
        .cannot.read.path('usersPublic');
    expect(targaryen.users.unauthenticated)
        .cannot.write.to.path('usersPublic');
});
it('should let authenticated users read and write', function () {

    // Any user.
    expect(targaryen.users.password)
        .can.read.path('usersPublic');
    expect(targaryen.users.password)
        .can.write.to.path('usersPublic');

    // Test user.
    expect(user1)
        .can.read.path('usersPublic');
    expect(user1)
        .can.write.to.path('usersPublic');
});
it('should let admin users read and write', function () {

    // Admin user.
    expect(admin)
        .can.read.path('usersPublic');
    expect(admin)
        .can.write.to.path('usersPublic');
});
it('should validate user writes', function () {

    // Stage Firebase.
    targaryen.setFirebaseData({
        "usersPrivate": {
            "-noVis01": {
                "email": "rusty@nutts.com",
                "beta": {
                    "zipCode": 55102
                },
                "tAndA": true,
                "public": "-pubPers01",
                "created": "Sun Dec 28 2015 20:08:00 -0600"
            },
            "-noVis02": {
                "email": "rusty@nutts.com",
                "beta": {
                    "zipCode": 55102
                },
                "tAndA": true,
                "public": "-pubPers02",
                "created": "Sun Dec 28 2015 20:08:00 -0600"
            }
        },
        "usersPublic": {
            "-pubPers01": {
                "userName": "-uname0",
                "firstName": "Russell",
                "lastName": "Bunch",
                "birthday": "Dec 28 2015",
                "gender": "male",
                "bio": "I like turtles",
                "created": "Sun Dec 28 2015 20:08:00 -0600"
            }
        },
        "userNames": {
            "wizrad69": {
                "active": true,
                "user": "-pubPers01"
            },
            "lolz0r": {
                "active": true,
                "user": "-pubPers02"
            }
        }
    });

    // Typical node.
    expect(user2)
        .can.write({
        "userName": "lolz0r",
        "firstName": "Russell",
        "lastName": "Bunch",
        "birthday": "Dec 28 2015",
        "gender": "male",
        "bio": "I like turtles",
        "created": "Sun Dec 28 2015 20:08:00 -0600"
    }).to.path('usersPublic/-pubPers02');

    // Data exists.
    expect(user1)
        .cannot.write({
        "userName": "-ptpeople01",
        "firstName": "Russell",
        "lastName": "Bunch",
        "birthday": "Dec 28 2015",
        "gender": "male",
        "created": "Sun Dec 28 2015 20:08:00 -0600"
    }).to.path('usersPublic/-pubPers01');

    // Missing data.
    expect(user2)
        .cannot.write({
        "birthday": "Dec 28 2015",
        "gender": "male"
    }).to.path('usersPublic/-pubPers02');

    // Update bio.
    expect(user2)
        .can.write({
        "bio": "I like turtles"
    }).to.path('usersPublic/-pubPers01');

    // Unrecognized key.
    expect(user1)
        .cannot.write({
            "peanut": "butter"
    }).to.path('usersPublic/-pubPers01');

    // Inaccessible data space..
    expect(user1)
        .cannot.write({
            "butter": "jelly time"
    }).to.path('usersPublic/peanut');
});