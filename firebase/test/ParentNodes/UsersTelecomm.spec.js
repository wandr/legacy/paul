/**
 * Created by Russell on 1/23/2016.
 */
/*jslint node: true */
"use strict";
var chai = require('chai'),
    expect = chai.expect,
    targaryen = require('targaryen');
chai.use(targaryen.chai);

// Auths.
var user = {uid: '-noVis01', provider: 'password'};
var admin = {uid: 'admin', master: true, provider: 'custom'};

// Tests.
it('shouldn\'t let unauthenticated users read or write', function () {
    expect(targaryen.users.unauthenticated)
        .cannot.read.path('usersTelecomm');
    expect(targaryen.users.unauthenticated)
        .cannot.write.to.path('usersTelecomm');
});
it('should let authenticated users write but should allow read', function () {

    // Any user.
    expect(targaryen.users.password)
        .can.read.path('usersTelecomm');
    expect(targaryen.users.password)
        .can.write.to.path('usersTelecomm');

    // Test user.
    expect(user)
        .can.read.path('usersTelecomm');
    expect(user)
        .can.write.to.path('usersTelecomm');
});
it('should let admin users read and write', function () {

    // Admin user.
    expect(admin)
        .can.read.path('usersTelecomm');
    expect(admin)
        .can.write.to.path('usersTelecomm');
});
it('should validate user writes', function () {

    // Stage Firebase
    targaryen.setFirebaseData({
        "usersTelecomm": {
            "-pubPers02": {
                "CC": 1,
                "tele": "someHash"
            }
        }
    });

    // Typical node.
    expect(user)
        .can.write({
        "CC": 1,
        "tele": "bcaf24141"
    }).to.path('usersTelecomm/-pubPers01');

    // Wrong data types.
    expect(user)
        .cannot.write({
        "CC": "1",
        "tele": 2
    }).to.path('usersTelecomm/-pubPers01');

    // Only server can write verified.
    expect(user)
        .cannot.write({
        "CC": "1",
        "tele": "someHash",
        "verified": true
    }).to.path('usersTelecomm/-pubPers01');

    // Unrecognized key.
    expect(user)
        .cannot.write({
        "peanut": "someShit"
    }).to.path('usersTelecomm/-pubPers01');

    // Inaccessible data space.
    expect(user)
        .cannot.write({
        "butter": "jelly time"
    }).to.path('usersTelecomm/peanut');
    // TODO Validate numbers
});

