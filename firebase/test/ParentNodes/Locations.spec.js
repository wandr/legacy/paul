/**
 * Created by Russell on 1/23/2016.
 */
/*jslint node: true */
"use strict";
var chai = require('chai'),
    expect = chai.expect,
    targaryen = require('targaryen');
chai.use(targaryen.chai);

// Auths.
var user = {uid: '-noVis01', provider: 'password'};
var admin = {uid: 'admin', master: true, provider: 'custom'};

// Tests.
it('shouldn\'t let unauthenticated users read or write', function () {
    expect(targaryen.users.unauthenticated)
        .cannot.read.path('locations');
    expect(targaryen.users.unauthenticated)
        .cannot.write.to.path('locations');
});
it('should let authenticated usersread and write', function () {

    // Any user.
    expect(targaryen.users.password)
        .can.read.path('locations');
    expect(targaryen.users.password)
        .can.write.to.path('locations');

    // Test user.
    expect(user)
        .can.read.path('locations');
    expect(user)
        .can.write.to.path('locations');
});
it('should let admin users read and write', function () {

    // Admin user.
    expect(admin)
        .can.read.path('locations');
    expect(admin)
        .can.write.to.path('locations');
});
