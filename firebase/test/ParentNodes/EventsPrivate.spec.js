/**
 * Created by Russell on 1/23/2016.
 */
/*jslint node: true */
"use strict";
var chai = require('chai'),
    expect = chai.expect,
    targaryen = require('targaryen');
chai.use(targaryen.chai);

// Auths.
var user = {uid: '-noVis01', provider: 'password'};
var admin = {uid: 'admin', master: true, provider: 'custom'};

// Tests.
it('shouldn\'t let unauthenticated users read or write', function () {
    expect(targaryen.users.unauthenticated)
        .cannot.read.path('eventsPrivate');
    expect(targaryen.users.unauthenticated)
        .cannot.write.to.path('eventsPrivate');
});
it('shouldn\'t let authenticated users read but should allow write', function () {

    // Any user.
    expect(targaryen.users.password)
        .cannot.read.path('eventsPrivate');
    expect(targaryen.users.password)
        .can.write.to.path('eventsPrivate');

    // Test user.
    expect(user)
        .cannot.read.path('eventsPrivate');
    expect(user)
        .can.write.to.path('eventsPrivate');
});
it('should let admin users read and write', function () {

    // Admin user.
    expect(admin)
        .can.read.path('eventsPrivate');
    expect(admin)
        .can.write.to.path('eventsPrivate');
});
it('should validate user writes', function () {

    // Stage Firebase.
    targaryen.setFirebaseData({
        "categories": {
            "-cat0": {
                "parent": "Music",
                "child": "Festival"
            }
        },
        "locations": {
            "-noVisEvent01": {
                ".priority": "9q5cf3pmqq",
                "g": "9q5cf3pmqq",
                "l": [34.064048, -118.36746]
            }
        },
        "usersPrivate": {
            "-noVis01": {
                "email": "rusty@nutts.com",
                "beta": {
                    "zipCode": 55102
                },
                "tAndA": true,
                "public": "-pubPers01",
                "created": "Sun Dec 28 2015 20:08:00 -0600"
            }
        },
        "usersPublic": {
            "-pubPers01": {
                "userName": "-uname0",
                "firstName": "Russell",
                "lastName": "Bunch",
                "birthday": "Dec 28 2015",
                "gender": "male",
                "bio": "I like turtles",
                "created": "Sun Dec 28 2015 20:08:00 -0600"
            }
        },
        "userNames": {
            "-uname0": {
                "userName": "wizrad69b0ner",
                "active": true,
                "user": "-pubPers01"
            }
        }
    });

    // Typical node.
    expect(user)
        .can.write({
        "owner": "-pubPers01",
        "name": "This is an event",
        "description": "I like turtles at this event",
        "addressName": "My house",
        "address": "711 Butternut Avenue Saint Paul, MN 55102",
        "startTime": "Sun Dec 28 2015 20:08:00 -0600",
        "endTime": "Sun Dec 28 2015 22:00:00 -0600",
        "category": "-cat0",
        "prices": {
            "-price01": {
                "name": "Keg tax",
                "cost": 5
            }
        }
    }).to.path('eventsPrivate/-noVisEvent01');

    // Category doesn't exist.
    expect(user)
        .cannot.write({
        "owner": "-pubPers01",
        "name": "This is an event",
        "description": "I like turtles at this event",
        "addressName": "My house",
        "address": "711 Butternut Avenue Saint Paul, MN 55102",
        "startTime": "Sun Dec 28 2015 20:08:00 -0600",
        "endTime": "Sun Dec 28 2015 22:00:00 -0600",
        "category": "-noExist"
    }).to.path('eventsPrivate/-noVisEvent01');

    // Owner doesn't exist.
    expect(user)
        .cannot.write({
        "owner": "-noExist",
        "name": "This is an event",
        "description": "I like turtles at this event",
        "addressName": "My house",
        "address": "711 Butternut Avenue Saint Paul, MN 55102",
        "startTime": "Sun Dec 28 2015 20:08:00 -0600",
        "endTime": "Sun Dec 28 2015 22:00:00 -0600",
        "category": "-cat0"
    }).to.path('eventsPrivate/-noVisEvent01');

    // No matching location.
    expect(user)
        .cannot.write({
        "owner": "-pubPers01",
        "name": "This is an event",
        "description": "I like turtles at this event",
        "addressName": "My house",
        "address": "711 Butternut Avenue Saint Paul, MN 55102",
        "startTime": "Sun Dec 28 2015 20:08:00 -0600",
        "endTime": "Sun Dec 28 2015 22:00:00 -0600",
        "category": "-cat0",
        "prices": {
            "-price01": {
                "name": "Keg tax",
                "cost": 5
            }
        }
    }).to.path('eventsPrivate/-noVisEvent02');

    // TODO Edit existing event.
    // TODO Cannot overwrite event
    // TODO Can write to owned event.
    // TODO Can read from owned event.
    // TODO Can read from event invited to.
});
