/*jslint node: true */
"use strict";
var chai = require('chai'),
    expect = chai.expect,
    targaryen = require('targaryen');
chai.use(targaryen.chai);

// Auths.
var user = {uid: '-noVis01', provider: 'password'};
var legal = {uid: 'admin', wandrRole: 'legal', provider: 'custom'};
var admin = {uid: 'admin', master: true, provider: 'custom'};

// Tests.
it('shouldn\'t let unauthenticated users read or write', function () {
    expect(targaryen.users.unauthenticated)
        .cannot.read.path('legal');
    expect(targaryen.users.unauthenticated)
        .cannot.write.to.path('legal');
});
it('shouldn\'t let authenticated users write but should allow read', function () {

    // Any user.
    expect(targaryen.users.password)
        .can.read.path('legal');
    expect(targaryen.users.password)
        .cannot.write.to.path('legal');

    // Test user.
    expect(user)
        .can.read.path('legal');
    expect(user)
        .cannot.write.to.path('legal');
});
it('should let the legal user read and write', function () {

    // Official legal user.
    expect(legal)
        .can.read.path('legal');
    expect(legal)
        .can.write.to.path('legal');

});
it('should validate legal user writes', function () {

    // Empty the Firebase.
    targaryen.setFirebaseData({});

    // Typical node.
    expect(legal).can.write({
        "tAndA": "Lorem ipsom poop",
        "active": true
    }).to.path('legal/tAndAs/-tAndA0');

    // Typical node.
    expect(legal).can.write({
        "tAndA": "Lorem ipsom poop",
        "active": false
    }).to.path('legal/tAndAs/-tAndA0');

    // Requires boolean.
    expect(legal).cannot.write({
        "tAndA": "Lorem ipsom poop",
        "active": "true"
    }).to.path('legal/tAndAs/-tAndA0');

    // Unrecognized key.
    expect(legal).cannot.write({
        "bestEver": false
    }).to.path('legal/tAndAs/-tAndA0');

    // Inaccessible data space.
    expect(legal).cannot.write({
        "butter": true
    }).to.path('legal/peanut');
});