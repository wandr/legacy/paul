/**
 * Created by Russell on 1/23/2016.
 */
/*jslint node: true */
"use strict";
var chai = require('chai'),
    expect = chai.expect,
    targaryen = require('targaryen');
chai.use(targaryen.chai);

// Auths.
var user = {uid: '-noVis01', provider: 'password'};
var beta = {uid: 'admin', wandrRole: 'beta', provider: 'custom'};

// Tests.
it('shouldn\'t let unauthenticated users read or write', function () {
    expect(targaryen.users.unauthenticated)
        .cannot.read.path('beta');
    expect(targaryen.users.unauthenticated)
        .cannot.write.to.path('beta');
});
it('shouldn\'t let authenticated users read or write', function () {

    // Any user.
    expect(targaryen.users.password)
        .cannot.read.path('beta');
    expect(targaryen.users.password)
        .cannot.read.path('beta/zipCodes');
    expect(targaryen.users.password)
        .cannot.write.to.path('beta');
    expect(targaryen.users.password)
        .cannot.write.to.path('beta/zipCodes');

    // Test user.
    expect(user)
        .cannot.read.path('beta');
    expect(user)
        .cannot.read.path('beta/zipCodes');
    expect(user)
        .cannot.write.to.path('beta');
    expect(user)
        .cannot.write.to.path('beta/zipCodes');
});
it('should let beta user read and write', function () {

    // Official beta user.
    expect(beta)
        .can.read.path('beta');
    expect(beta)
        .can.write.to.path('beta');
});
it('should validate beta user writes', function () {

    // Empty the Firebase.
    targaryen.setFirebaseData({});

    // Typical node.
    expect(beta).can.write({
        "55102": {
            "enabled": true
        }
    }).path('beta/zipCodes');

    // Typical node.
    expect(beta).can.write({
        "55102": {
            "enabled": false
        }
    }).to.path('beta/zipCodes');

    // Require boolean.
    expect(beta).cannot.write({
        "55102": {
            "enabled": "false"
        }
    }).to.path('beta/zipCodes');

    // Unrecognized key.
    expect(beta).cannot.write({
        "55102": {
            "peanut": "oh yeah"
        }
    }).to.path('beta/zipCodes');

    // Inaccessible data space.
    expect(beta).cannot.write({
        "butter": true
    }).to.path('beta/peanut');
});