/**
 * Created by Russell on 1/23/2016.
 */
/*jslint node: true */
"use strict";
var chai = require('chai'),
    expect = chai.expect,
    targaryen = require('targaryen');
chai.use(targaryen.chai);

// Auths.
var user = {uid: '-noVis01', provider: 'password'};
var cat = {uid: 'admin', wandrRole: 'cat', provider: 'custom'};

// Tests.
it('shouldn\'t let unauthenticated users read or write', function () {
    expect(targaryen.users.unauthenticated)
        .cannot.read.path('categories');
    expect(targaryen.users.unauthenticated)
        .cannot.write.to.path('categories');
});
it('shouldn\'t let authenticated users write but should allow read', function () {

    // Any user.
    expect(targaryen.users.password)
        .can.read.path('categories');
    expect(targaryen.users.password)
        .cannot.write.to.path('categories');

    // Test user.
    expect(user)
        .can.read.path('categories');
    expect(user)
        .cannot.write.to.path('categories');
});
it('should let admin users read and write', function () {

    // Official cat user.
    expect(cat)
        .can.read.path('categories');
    expect(cat)
        .can.write.to.path('categories');
});
it('should validate cat user writes', function () {

    // Empty the Firebase.
    targaryen.setFirebaseData({});

    // Typical node.
    expect(cat).can.write({
        "parent": "Music",
        "child": "Festival"
    }).path('categories/-cat0');

    // Missing key.
    expect(cat).cannot.write({
        "parent": "Music"
    }).to.path('categories/-cat0');

    // Missing key.
    expect(cat).cannot.write({
        "child": "Music"
    }).to.path('categories/-cat0');

    // Unrecognized key.
    expect(cat).cannot.write({
        "peanut": "butter"
    }).to.path('categories/-cat0');

    // Inaccessible data space.
    expect(cat).cannot.write({
        "butter": "jelly time"
    }).to.path('categories/peanut');
});