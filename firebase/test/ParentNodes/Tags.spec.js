/**
 * Created by Russell on 1/23/2016.
 */
/*jslint node: true */
"use strict";
var chai = require('chai'),
    expect = chai.expect,
    targaryen = require('targaryen');
chai.use(targaryen.chai);

// Auths.
var user = {uid: '-noVis01', provider: 'password'};
var admin = {uid: 'admin', master: true, provider: 'custom'};

// Tests.
it('shouldn\'t let unauthenticated users read or write', function () {
    expect(targaryen.users.unauthenticated)
        .cannot.read.path('tags');
    expect(targaryen.users.unauthenticated)
        .cannot.write.to.path('tags');
});
it('shouldn\'t let authenticated users write but should allow read', function () {

    // Any user.
    expect(targaryen.users.password)
        .can.read.path('tags');
    expect(targaryen.users.password)
        .can.write.to.path('tags');

    // Test user.
    expect(user)
        .can.read.path('tags');
    expect(user)
        .can.write.to.path('tags');
});
it('should let admin users read and write', function () {

    // Admin user.
    expect(admin)
        .can.read.path('tags');
    expect(admin)
        .can.write.to.path('tags');
});
it('should validate user writes', function () {
    
    // Empty the Firebase.
    targaryen.setFirebaseData({
        "tags": {
            "-tag0": {
                "tag": "#peanut",
                "used": 1,
                "firstUser": "-pubPers02"
            }
        }
    });

    // Typical node.
    expect(user).can.write({
        "tag": "#peanut",
        "used": 1,
        "firstUser": "-pubPers01"
    }).to.path('tags/-tag1');

    // Typical node.
    expect(user).can.write({
        "used": 2
    }).to.path('tags/-tag0');

    // Can't change existing tag.
    expect(user).cannot.write({
        "tag": "#butter"
    }).to.path('tags/-tag0');

    // Requires number.
    expect(user).cannot.write({
        "used": "2"
    }).to.path('tags/-tag0');

    // Can't change last user
    expect(user).cannot.write({
        "firstUser": "-pubPers01"
    }).to.path('tags/-tag0');

    // Max length 20.
    expect(user).cannot.write({
        "tag": "#peanutpeanutpeanutpeanutpeanute",
        "used": 10,
        "firstUser": "-pubPers01"
    }).to.path('tags/-tag1');

    // Missing hash tag.
    expect(user).cannot.write({
        "tag": "peanut",
        "used": 10,
        "firstUser": "-pubPers01"
    }).to.path('tags/-tag1');

    // Unrecognized key.
    expect(user).cannot.write({
        "butter": "jelly time"
    }).to.path('tags/-tag0');

    // Inaccessible data space.
    expect(user).cannot.write({
        "butter": "jelly time"
    }).to.path('tags/peanut');
});
