/**
 * Created by Russell on 1/23/2016.
 */
/*jslint node: true */
"use strict";
var chai = require('chai'),
    expect = chai.expect,
    targaryen = require('targaryen');
chai.use(targaryen.chai);

// Auths.
var user = {uid: '-noVis01', provider: 'password'};
var admin = {uid: 'admin', master: true, provider: 'custom'};

// Tests.
it('shouldn\'t let unauthenticated users read or write', function () {
    expect(targaryen.users.unauthenticated)
        .cannot.read.path('userNames');
    expect(targaryen.users.unauthenticated)
        .cannot.write.to.path('userNames');
});
it('should let authenticated users read and write', function () {

    // Any user.
    expect(targaryen.users.password)
        .can.read.path('userNames');
    expect(targaryen.users.password)
        .can.write.to.path('userNames');

    // Test user.
    expect(user)
        .can.read.path('userNames');
    expect(user)
        .can.write.to.path('userNames');
});
it('should let admin users read and write', function () {

    // Admin user.
    expect(admin)
        .can.read.path('userNames');
    expect(admin)
        .can.write.to.path('userNames');
});
it('should validate user writes', function () {

    // Stage Firebase.
    targaryen.setFirebaseData({
        "userNames": {
            "lolz0r": {
                "active": true,
                "user": "-pubPers20"
            }
        },
        "usersPublic": {
            "-pubPers01": true,
            "-pubPers20": true
        }
    });

    // Typical node.
    expect(user)
        .can.write({
        "active": true,
        "user": "-pubPers01"
    }).to.path('userNames/wizrad69');

    // Username taken.
    expect(user)
        .cannot.write({
        "active": true,
        "user": "-pubPers01"
    }).to.path('userNames/lolz0r');

    // Requires boolean.
    expect(user)
        .cannot.write({
        "active": "false",
        "user": "-pubPers01"
    }).to.path('userNames/wizrad69');

    // User doesn't match.
    expect(user)
        .cannot.write({
        "active": "false",
        "user": "invalid",
    }).to.path('userNames/wizrad69');

    // Unrecognized key.
    expect(user)
        .cannot.write({
        "peanut": "butter"
    }).to.path('userNames/wizrad69');
});