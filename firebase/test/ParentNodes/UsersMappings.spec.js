/**
 * Created by Russell on 1/23/2016.
 */
/*jslint node: true */
"use strict";
var chai = require('chai'),
    expect = chai.expect,
    targaryen = require('targaryen');
chai.use(targaryen.chai);

// Auths.
var user = {uid: '-noVis01', provider: 'password'};
var admin = {uid: 'admin', master: true, provider: 'custom'};

// Tests.
it('shouldn\'t let unauthenticated users read or write', function () {
    expect(targaryen.users.unauthenticated)
        .cannot.read.path('usersMappings');
    expect(targaryen.users.unauthenticated)
        .cannot.write.to.path('usersMappings');
});
it('shouldn\'t let authenticated users read or write', function () {

    // Any user.
    expect(targaryen.users.password)
        .cannot.read.path('usersMappings');
    expect(targaryen.users.password)
        .can.write.to.path('usersMappings');

    // Test user.
    expect(user)
        .cannot.read.path('usersMappings');
    expect(user)
        .can.write.to.path('usersMappings');
});
it('should let admin users read and write', function () {
    
    // Admin user.
    expect(admin)
        .can.read.path('usersMappings');
    expect(admin)
        .can.write.to.path('usersMappings');
});
it('should validate user writes', function () {

    // Stage Firebase.
    targaryen.setFirebaseData({
        "eventsPublic": {
            "-event0": {
                "randomData": true
            }
        },
        "eventsPrivate": {
            "-event1": {
                "randomData": true
            }
        }
    });

    // Typical node.
    expect(user)
        .can.write({
        "events": {
            "-event0": true
        }
    }).to.path('usersMappings/-noVis01');

    // Typical node.
    expect(user)
        .can.write({
        "events": {
            "-event1": false
        }
    }).to.path('usersMappings/-noVis01');

    // Requires boolean.
    expect(user)
        .cannot.write({
        "events": {
            "-event0": "true"
        }
    }).to.path('usersMappings/-noVis01');

    // Unrecognized key.
    expect(user)
        .cannot.write({
        "peanut": "butter"
    }).to.path('usersMappings/-noVis01');

    // Inaccessible data space.
    expect(user)
        .cannot.write({
        "butter": "jelly time"
    }).to.path('usersMappings/peanut');
});
