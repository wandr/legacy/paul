/**
 * Created by Russell on 1/23/2016.
 */
/*jslint node: true */
"use strict";
var chai = require('chai'),
    expect = chai.expect,
    targaryen = require('targaryen');
chai.use(targaryen.chai);

// Auths.
var user1 = {uid: '-noVis01', provider: 'password'};
var user2 = {uid: '-noVis02', provider: 'password'};
var admin = {uid: 'admin', master: true, provider: 'custom'};

// Tests.
it('shouldn\'t let unauthenticated users read or write', function () {
    expect(targaryen.users.unauthenticated)
        .cannot.read.path('usersPrivate');
    expect(targaryen.users.unauthenticated)
        .cannot.write.to.path('usersPrivate');
});
it('shouldn\'t let authenticated users read but should let them write', function () {

    // Any user.
    expect(targaryen.users.password)
        .cannot.read.path('usersPrivate');
    expect(targaryen.users.password)
        .can.write.to.path('usersPrivate');

    // Test user.
    expect(user1)
        .cannot.read.path('usersPrivate');
    expect(user1)
        .can.write.to.path('usersPrivate');
});
it('should let admin users read and write', function () {

    // Admin user.
    expect(admin)
        .can.read.path('usersPrivate');
    expect(admin)
        .can.write.to.path('usersPrivate');
});
it('should validate user writes', function () {

    // Stage Firebase
    targaryen.setFirebaseData({
        "usersPrivate": {
            "-noVis02": {
                "email": "rusty@nutts.com",
                "beta": {
                    "zipCode": 55102
                },
                "tAndA": true,
                "public": "-pubPers02",
                "created": "Sun Dec 28 2015 20:08:00 -0600"
            }
        },
        "usersPublic": {
            "-pubPers02": {
                "userName": "-uname1",
                "firstName": "Russell",
                "lastName": "Bunch",
                "birthday": "Dec 28 2015",
                "gender": "male",
                "bio": "I like turtles",
                "created": "Sun Dec 28 2015 20:08:00 -0600"

            }
        },
        "userNames": {
            "-uname1": {
                "userName": "wizrad69",
                "active": true,
                "user": "-pubPers02"
            }
        }
    });

    // Typical node.
    expect(user1)
        .can.write({
        "email": "first@nutts.com",
        "beta": {
            "zipCode": 55102
        },
        "tAndA": true,
        "public": "-pubPers01",
        "created": "Sun Dec 28 2015 20:08:00 -0600"
    }).to.path('usersPrivate/-noVis01');

    // Typical update.
    expect(user2)
        .can.write({
        "beta": {
            "zipCode": 55102
        }
    }).to.path('usersPrivate/-noVis02');

    // Data exists
    expect(user1)
        .cannot.write({
        "email": "rusty@nutts.com",
        "beta": {
            "zipCode": 55102
        },
        "tAndA": true,
        "public": "-pubPers01",
        "created": "12/28/15 08:20 PM"
    }).to.path('usersPrivate/-noVis02');

    // Unrecognized key.
    expect(user1)
        .cannot.write({
        "peanut": "butter"
    }).to.path('usersPrivate/-noVis01');

    // Inaccessible data space.
    expect(user1)
        .cannot.write({
        "butter": "jelly time"
    }).to.path('usersPrivate/peanut');
});