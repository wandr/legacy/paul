/**
 * Created by Russell on 1/23/2016.
 */
/*jslint node: true */
"use strict";
var chai = require('chai'),
    expect = chai.expect,
    targaryen = require('targaryen');
chai.use(targaryen.chai);

// Auths.
var user = {uid: '-noVis01', provider: 'password'};
var admin = {uid: 'admin', master: true, provider: 'custom'};

// Tests.
it('shouldn\'t let unauthenticated users read or write', function () {
    expect(targaryen.users.unauthenticated)
        .cannot.read.path('eventTags');
    expect(targaryen.users.unauthenticated)
        .cannot.write.to.path('eventTags');
});
it('should authenticated users read and write', function () {

    // Any user.
    expect(targaryen.users.password)
        .can.read.path('eventTags');
    expect(targaryen.users.password)
        .can.write.to.path('eventTags');

    // Test user.
    expect(user)
        .can.read.path('eventTags');
    expect(user)
        .can.write.to.path('eventTags');
});
it('should let admin users read and write', function () {

    // Admin user.
    expect(admin)
        .can.read.path('eventTags');
    expect(admin)
        .can.write.to.path('eventTags');
});
it('should validate user writes', function () {

    // Stage Firebase.
    targaryen.setFirebaseData({
        "eventsPublic": {
            "-event0": {
                "randomData": true
            },
            "-event1": {
                "randomData": true
            }
        },
        "eventTags": {
            "-event0": {
                "-tag0": "#peanut"
            }
        },
        "tags": {
            "-tag0": {
                "tag": "#peanut",
                "used": 1,
                "firstUser": "-user20"
            },
            "-tag1": {
                "tag": "#butter",
                "used": 1,
                "firstUser": "-user21"
            }
        }
    });

    // Typical data.
    expect(user).can.write({
        "-tag1": "#butter"
    }).to.path('eventTags/-event0');

    // Typical data.
    expect(user).can.write({
        "-tag2": "#freedom"
    }).to.path('eventTags/-event1');

    // Require string.
    expect(user).can.write({
        "tag1": false
    }).to.path('eventTags/-event0');
});