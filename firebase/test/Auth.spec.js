/*jslint node: true */
"use strict";
var chai = require('chai'),
    assert = chai.assert,
    targaryen = require('targaryen');
chai.use(targaryen.chai);

describe('Firebase', function() {

    // Firebase.
    var Firebase = require("firebase");
    var FirebaseTokenGenerator = require('firebase-token-generator');
    describe('#no_api_key', function() {
        it('should be undefined', function() {
            assert.isUndefined(process.env.FB_API);
        });
    });
    describe('#can_not_auth', function() {
        it('should fail to authenticate with error, missing key', function(done) {
            // Config token and auth.
            var tokenGenerator = new FirebaseTokenGenerator('wrong');
            var token = tokenGenerator.createToken({uid: 'UnitTest'});
            var ref = new Firebase("https://wewandr-test.firebaseio.com/");
            ref.authWithCustomToken(token, function (error) {
                if (error) {
                    done();
                }
            });
        });
    });
});
