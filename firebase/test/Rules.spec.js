/*jslint node: true */
"use strict";
var chai = require('chai'),
    targaryen = require('targaryen');
chai.use(targaryen.chai);
describe('Test Rules & Schema', function () {
    before(function () {

        // Ensure the demo Firebase is empty and loaded with the defined rules.
        targaryen.setFirebaseData({});
        targaryen.setFirebaseRules(require('../Rules.json'));
    });

    // Test each parent node of Firebase. Each node has its own spec.js file.
    describe('#beta', function () {
        require('./ParentNodes/Beta.spec.js');
    });
    describe('#cat', function () {
        require('./ParentNodes/Categories.spec.js');
    });
    describe('#eventsPrivate', function () {
        require('./ParentNodes/EventsPrivate.spec.js');
    });
    describe('#eventsPublic', function () {
        require('./ParentNodes/EventsPublic.spec.js');
    });
    describe('#eventTags', function () {
        require('./ParentNodes/EventTags.spec.js');
    });
    describe('#legal', function () {
        require('./ParentNodes/Legal.spec.js');
    });
    describe('#locations', function () {
        require('./ParentNodes/Locations.spec.js');
    });
    describe('#tags', function () {
        require('./ParentNodes/Tags.spec.js');
    });
    describe('#userNames', function () {
        require('./ParentNodes/UserNames.spec.js');
    });
    describe('#usersMappings', function () {
        require('./ParentNodes/UsersMappings.spec.js');
    });
    describe('#usersPrivate', function () {
        require('./ParentNodes/UsersPrivate.spec.js');
    });
    describe('#usersPublic', function () {
        require('./ParentNodes/UsersPublic.spec.js');
    });
    describe('#usersTelecomm', function () {
        require('./ParentNodes/UsersTelecomm.spec.js');
    });
});